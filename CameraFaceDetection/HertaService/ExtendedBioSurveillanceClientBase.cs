﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Description;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Popups;

namespace FaceDetection.HertaService
{
    public partial class ExtendedBioSurveillanceServiceClientBase
    {
        static partial void ConfigureEndpoint(ServiceEndpoint serviceEndpoint, ClientCredentials clientCredentials)
        {
            if (serviceEndpoint.Name ==
                    EndpointConfiguration.NetTcpBinding_IExtendedBioSurveillanceService.ToString())
            {
                serviceEndpoint.Binding.SendTimeout = new System.TimeSpan(0, 1, 0);
                serviceEndpoint.Binding.Name = "MyTcpBinding";
                serviceEndpoint.Address = GetEndpointAddress(EndpointConfiguration.NetTcpBinding_IExtendedBioSurveillanceService);
            }
        }

        private static System.ServiceModel.EndpointAddress GetEndpointAddress(EndpointConfiguration endpointConfiguration)
        {
            var localSettings = Windows.Storage.ApplicationData.Current.LocalSettings;
            var ip = localSettings.Values["hertaIp"]?.ToString() ?? "localhost";
            var tcpPort = localSettings.Values["hertaPort"]?.ToString() ?? "8000";

            try {
                //string httpPort = "8000";
                if ((endpointConfiguration == EndpointConfiguration.NetTcpBinding_IExtendedBioSurveillanceService))
                {
                    return new System.ServiceModel.EndpointAddress($"net.tcp://{ip}:{tcpPort}/BioService/BioSurveillance");
                }
                if ((endpointConfiguration == EndpointConfiguration.NetTcpBinding_IExtendedBioSurveillanceService1))
                {
                    return new System.ServiceModel.EndpointAddress($"net.tcp://{ip}:{tcpPort}/BioService/BioSurveillanceNext");
                }
                if ((endpointConfiguration == EndpointConfiguration.NetTcpBinding_IExtendedBioSurveillanceService2))
                {
                    return new System.ServiceModel.EndpointAddress($"net.tcp://{ip}:{tcpPort}/BioService/BioFinder");
                }
                if ((endpointConfiguration == EndpointConfiguration.NetTcpBinding_IExtendedBioSurveillanceService3))
                {
                    return new System.ServiceModel.EndpointAddress($"net.tcp://{ip}:{tcpPort}/BioService/BioAccess");
                }
            }
            catch (System.UriFormatException)
            {
                localSettings.Values["hertaIp"] = "localhost";
                localSettings.Values["hertaPort"] = "8000";
                var md = new MessageDialog("Ip address and port set to default values due to invalid settings. Try to enter in proper values from settings window.");
                md.ShowAsync();
                return GetEndpointAddress(endpointConfiguration);
            }
            //if ((endpointConfiguration == EndpointConfiguration.HttpBinding_IExtendedBioSurveillanceService))
            //{
            //    return new System.ServiceModel.EndpointAddress($"http://{ip}:{httpPort}/BioService/BioAccess");
            //}
            throw new System.InvalidOperationException(string.Format("Could not find endpoint with name \'{0}\'.", endpointConfiguration));
            }


    }

    public partial class BasicBioSurveillanceServiceClient
    {
        static partial void ConfigureEndpoint(System.ServiceModel.Description.ServiceEndpoint serviceEndpoint, System.ServiceModel.Description.ClientCredentials clientCredentials)
        {
            if (serviceEndpoint.Name ==
                    EndpointConfiguration.BasicHttpBinding_IBasicBioSurveillanceService.ToString())
            {
                serviceEndpoint.Binding.SendTimeout = new System.TimeSpan(0, 1, 0);
                serviceEndpoint.Binding.Name = "MyHttpBinding";
                serviceEndpoint.Address = GetEndpointAddress(EndpointConfiguration.BasicHttpBinding_IBasicBioSurveillanceService);
            }
        }

        private static System.ServiceModel.EndpointAddress GetEndpointAddress(EndpointConfiguration endpointConfiguration)
        {
            string ipNumber = "172.20.10.6";
            string port = "8000";

            if ((endpointConfiguration == EndpointConfiguration.BasicHttpBinding_IBasicBioSurveillanceService))
            {
                return new System.ServiceModel.EndpointAddress($"http://{ipNumber}:{port}/BioService/BioSurveillance");
            }
            if ((endpointConfiguration == EndpointConfiguration.BasicHttpBinding_IBasicBioSurveillanceService1))
            {
                return new System.ServiceModel.EndpointAddress($"http://{ipNumber}:{port}/BioService/BioSurveillanceNext");
            }
            if ((endpointConfiguration == EndpointConfiguration.BasicHttpBinding_IBasicBioSurveillanceService2))
            {
                return new System.ServiceModel.EndpointAddress($"http://{ipNumber}:{port}/BioService/BioFinder");
            }
            if ((endpointConfiguration == EndpointConfiguration.BasicHttpBinding_IBasicBioSurveillanceService3))
            {
                return new System.ServiceModel.EndpointAddress($"http://{ipNumber}:{port}/BioService/BioAccess");
            }
            throw new System.InvalidOperationException(string.Format("Could not find endpoint with name \'{0}\'.", endpointConfiguration));
        }      
    }
}

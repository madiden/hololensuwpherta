﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage.Streams;
using Windows.UI.Xaml.Media.Imaging;

namespace FaceDetection.HertaService
{
    public class Utilities
    {
        public static byte[] BitmapToByte(Bitmap bmp)
        {
            if (bmp == null)
            {
                return null;
            }
            MemoryStream memoryStream = new MemoryStream();
            bmp.Save(memoryStream, ImageFormat.Jpeg);
            return memoryStream.GetBuffer();
        }

        public static Bitmap ByteToBitmap(byte[] buffer)
        {
            if (buffer == null)
            {
                return null;
            }
            return (Bitmap)Image.FromStream(new MemoryStream(buffer));
        }

        public async static Task<BitmapImage> ImageFromBytes(byte[] bytes)
        {
            BitmapImage image = new BitmapImage();
            using (InMemoryRandomAccessStream stream = new InMemoryRandomAccessStream())
            {
                await stream.WriteAsync(bytes.AsBuffer());
                stream.Seek(0);
                await image.SetSourceAsync(stream);
            }
            return image;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Media;

namespace FaceDetection
{
    public class IdentifyResult
    {
        public ImageSource DetectedFace { get; set; }

        public ImageSource SubjectPhoto { get; set; }

        public string Score { get; set; }

        public int ScoreAsNumber
        {
            get
            {
                if (Double.TryParse(Score, out var number))
                    return (int)number;
                return 0;
            }
        }

        public string SubjectInfo { get; set; } 
    }
}
